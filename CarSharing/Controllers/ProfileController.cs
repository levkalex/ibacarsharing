﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CarSharing.Models.Database;
using CarSharing.Models.Identity;
using CarSharing.Models.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace CarSharing.Controllers
{
    [Authorize]
    public class ProfileController : Controller
    {
        private readonly UserManager<User> _userManager;
        IOrderRepository orderRepository;

        public ProfileController(UserManager<User> userManager,  IOrderRepository orderRepository)
        {
            _userManager = userManager;
            this.orderRepository = orderRepository;
        }

        public async Task<IActionResult> Index()
        {
            User user = await _userManager.GetUserAsync(User);

            var orders = orderRepository.Orders.Where(x => x.User == user);

            return View(orders);
        }

        public async Task<IActionResult> InProgressOrders()
        {
            User user = await _userManager.GetUserAsync(User);

            var orders = orderRepository.Orders.Include(x => x.Car).Include(x => x.Car.Brand).Where(x => x.User == user && x.Status == "in-progress").OrderByDescending(x => x.OrderId);

            return View(orders);
        }

        public async Task<IActionResult> WaitApproveOrders()
        {
            User user = await _userManager.GetUserAsync(User);

            var orders = orderRepository.Orders.Include(x => x.Car).Include(x => x.Car.Brand).Where(x => x.User == user && x.Status == "wait-approve").OrderByDescending(x => x.OrderId);

            return View(orders);
        }

        public async Task<IActionResult> PerformedOrders()
        {
            User user = await _userManager.GetUserAsync(User);

            var orders = orderRepository.Orders.Include(x => x.Car).Include(x => x.Car.Brand).Where(x => x.User == user && x.Status == "performed").OrderByDescending(x=>x.OrderId);

            return View(orders);
        }


        public async Task<IActionResult> UpcomingOrders()
        {
            User user = await _userManager.GetUserAsync(User);

            var orders = orderRepository.Orders.Include(x=>x.Car).Include(x=>x.Car.Brand).Where(x => x.User == user && (x.Status == "return-possible" || x.Status == "return-impossible" || x.Status == "waiting-payment")).OrderByDescending(x => x.OrderId);

            return View(orders);
        }

        public async Task<IActionResult> CanceledOrders()
        {
            User user = await _userManager.GetUserAsync(User);

            var orders = orderRepository.Orders.Include(x => x.Car).Include(x => x.Car.Brand).Where(x => x.User == user && x.Status == "canceled").OrderByDescending(x => x.OrderId);

            return View(orders);
        }

        public async Task<IActionResult> Info()
        {
            User user = await _userManager.GetUserAsync(User);

            return View(user);
        }

    }
}