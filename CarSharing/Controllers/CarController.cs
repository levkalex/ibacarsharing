﻿using CarSharing.Models;
using CarSharing.Models.Project;
using CarSharing.Models.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace CarSharing.Controllers
{
    [ApiExplorerSettings(IgnoreApi = true)]
    public class CarController : Controller
    {
        private IRepository repository;

        public CarController(IRepository repo)
        {
            repository = repo;
        }


        [Route("Car/{id:int}")]
        public ActionResult Car(int id)
        {
            Car car = repository.Cars.AsQueryable().Where(x => x.CarID == id).Include(x => x.Brand).FirstOrDefault();

            if (car != null)
                return View(car);
            else
                return NotFound();
        }


        public ActionResult List(string type = null, string brand = null)
        {
            var cars = repository.Cars.AsQueryable().Include(x => x.Brand);
            if (type != null) cars = cars.Where(x => x.Type == type).Include(x => x.Brand);
            if (brand != null) cars = cars.Where(x => x.Brand.Name == brand).Include(x => x.Brand);

            if (cars == null)
                return NotFound();

            var ListBrand = repository.Brands;
            var ListTypes = repository.Cars.Select(x => x.Type).Distinct();

            return View(new ListCarViewModel {  Cars = cars, Brands = ListBrand, Types = ListTypes, CurrentBrand = brand, CurrentType = type });
        }

        [HttpGet]
        public ViewResult Add()
        {
            var brands = repository.Brands;
            return View(brands);
        }

        [HttpPost]
        public async Task<ActionResult> Add(Car car, IFormFile file)
        {
            int id = repository.AddCar(car);

            if (file != null)
            {
                string path = @"wwwroot/images/model/" + id + ".jpg";
                using (var fileStream = new FileStream(path, FileMode.Create))
                {
                    await file.CopyToAsync(fileStream);
                }
            }

            return RedirectToAction("Car", "Car", new { id = car.CarID });
        }

        [HttpGet]
        public ViewResult Edit(int id)
        {
            Car car = repository.Cars.AsQueryable().Where(x => x.CarID == id).FirstOrDefault();
            var brands = repository.Brands;

            return View(new CarWithBrand(car, brands));
        }


        [HttpPost]
        public async Task<ActionResult> Edit(Car car, IFormFile file)
        {
            if (file != null)
            {
                string path = @"wwwroot/images/model/" + car.CarID + ".jpg";

                using (var fileStream = new FileStream(path, FileMode.Create))
                {
                    await file.CopyToAsync(fileStream);
                }
            }

            repository.UpdateCar(car);

            return RedirectToAction("Car", "Car", new { id = car.CarID });
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            Car car = repository.Cars.FirstOrDefault(x => x.CarID == id);

            if (car == null)
                return NotFound();
            else
                repository.DeleteCar(car);

            string path = @"wwwroot/images/model/" + id + ".jpg";
            if (System.IO.File.Exists(path))
            {
                System.IO.File.Delete(path);
            }

            return RedirectToAction("Index", "Home");
        }
    }
}
