﻿using CarSharing.Models.Database;
using CarSharing.Models.Identity;
using CarSharing.Models.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace CarSharing.Controllers
{
    [ApiExplorerSettings(IgnoreApi = true)]
    public class AccountController : Controller
    {
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly IStringLocalizer<AccountController> _localizer;
        IOrderRepository orderRepository;

        public AccountController(UserManager<User> userManager, SignInManager<User> signInManager, IStringLocalizer<AccountController> localizer, IOrderRepository orderRepository)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _localizer = localizer;
            this.orderRepository = orderRepository;
        }

        [HttpGet]
        public IActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Register(RegisterViewModel model)
        {
            if (model.Birthday > DateTime.Now.AddYears(-21) || model.Birthday < DateTime.Now.AddYears(-65))
            {
                ModelState.AddModelError("Birthday", _localizer["DateNotValid"]);
            }
            if (model.FirstName == string.Empty || model.LastName == string.Empty)
            {
                ModelState.AddModelError("FirstName", "Поля имя и фамилия обязательны к заполнению");
            }

            if (ModelState.IsValid)
            {
                User user = new User 
                { 
                    Email = model.Email, 
                    UserName = model.Email, 
                    Birthday = model.Birthday, 
                    FirstName = model.FirstName, 
                    LastName = model.LastName 
                };

                var result = await _userManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    await _signInManager.SignInAsync(user, false);
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    foreach (var error in result.Errors)
                    {
                        ModelState.AddModelError(string.Empty, _localizer[error.Code]);
                    }
                }
            }
            return View(model);
        }

        [HttpGet]
        public IActionResult Login(string returnUrl = null)
        {
            return View(new LoginViewModel { ReturnUrl = returnUrl });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                var result = await _signInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, false);
                if (result.Succeeded)
                {
                   /* var claimIdentity = new ClaimsIdentity { Claims = new List<Claim> { new Claim("FirstName", result.fir) } }
                    {
                        Claims = new List<Claim> { new Claim { }
                    User.AddIdentity(new ClaimsIdentity { Claims = new  )*/

                    if (!string.IsNullOrEmpty(model.ReturnUrl) && Url.IsLocalUrl(model.ReturnUrl))
                    {
                        return Redirect(model.ReturnUrl);
                    }
                    else
                    {
                        return RedirectToAction("Index", "Home");
                    }
                }
                else
                {
                    ModelState.AddModelError("", _localizer["errorlogin"]);
                }
            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> LogOff()
        {
            await _signInManager.SignOutAsync();
            return RedirectToAction("Index", "Home");
        }

       
    }
}

