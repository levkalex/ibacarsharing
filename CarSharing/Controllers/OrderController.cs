﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CarSharing.Models;
using CarSharing.Models.Database;
using CarSharing.Models.Identity;
using CarSharing.Models.Project;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Localization;
using static Microsoft.AspNetCore.Hosting.Internal.HostingApplication;

namespace CarSharing.Controllers
{
    [ApiExplorerSettings(IgnoreApi = true)]
    public class OrderController : Controller
    {
        private IOrderRepository repository;
        private readonly UserManager<User> _userManager;
        private readonly IStringLocalizer<OrderController> _localizer;
        private IRepository carRepository;

        public OrderController(IOrderRepository repo, UserManager<User> userManager, IRepository carRepo, IStringLocalizer<OrderController> localizer)
        {
            repository = repo;
            _userManager = userManager;
            carRepository = carRepo;
            _localizer = localizer;
        }

        [Route("Order")]
        public async Task<IActionResult> Order(int id, string returnUrl = null)
        {
            User user = await _userManager.GetUserAsync(User);

            var order = repository.Orders.Include(x => x.Car).Include(x => x.Car.Brand).FirstOrDefault(x => x.User == user && x.OrderId == id);

            ViewBag.returnUrl = returnUrl;

            if (order == null)
                return NotFound();
            else if (order.TypeOrder == "order")
                return View(order);
            else if (order.TypeOrder == "reserve")
                return View("Reserve", order);
            else
                return NotFound();
        }

        [HttpGet]
        [Authorize]
        public ActionResult Car(int id)
        {

            Car car = carRepository.Cars.AsQueryable().Where(x => x.CarID == id).Include(x => x.Brand).FirstOrDefault();

            if (car != null)
                return View(car);
            else
                return NotFound();
        }


        [HttpGet]
        public ActionResult Car2(int id)
        {

            Car car = carRepository.Cars.AsQueryable().Where(x => x.CarID == id).Include(x => x.Brand).FirstOrDefault();

            if (car != null)
                return View(car);
            else
                return NotFound();
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> Car(Order order)
        {
            if (order.DatetimeStartOrder < DateTime.Now)
            {
                ModelState.AddModelError(string.Empty, _localizer["Error! Order date can not be later!"]);
            }
            if ((order.DatetimeStartOrder - DateTime.Now).TotalHours < 36 && order.TypeOrder == "reserve")
            {
                ModelState.AddModelError(string.Empty, _localizer["Error! Reservations are allowed at least 36 hours before the start of the rental!"]);
            }
            if (order.DatetimeStartOrder > order.DatetimeFinishOrder)
            {
                ModelState.AddModelError(string.Empty, _localizer["Error! Date of the beginning and end of the lease is incorrect!"]);
            }
            if (!checkFreeTime(order.DatetimeStartOrder, order.DatetimeFinishOrder, order.CarId))
            {
                ModelState.AddModelError(string.Empty, _localizer["Error! This auto is busy at this time! Try another time!"]);
            }
            if (Math.Round(order.UnitCost * order.RentalTime, 2) != order.TotalCost)
            {
                ModelState.AddModelError(string.Empty, _localizer["An error has occurred! Try again!"]);
            }
            if (order.RentalTime == 0)
            {
                ModelState.AddModelError(string.Empty, _localizer["An error has occurred! Try again!"]);
            }
            if (ModelState.IsValid)
            {
                order.DateTimeOrder = DateTime.Now;

                User user = await _userManager.GetUserAsync(User);
                order.UserId = user.Id;

                

                repository.AddOrder(order);

                if (order.TypeOrder == "order")
                    return RedirectToAction("Info", new { info = _localizer["Order is sucsses."] });
                else
                    return RedirectToAction("Info", new { info = _localizer["Rent is sucsses."] });
            }

            return View(carRepository.Cars.AsQueryable().Where(x => x.CarID == order.CarId).Include(x => x.Brand).FirstOrDefault());
        }

        [HttpPost]
        public async Task<IActionResult> CanceledOrder(Order order)
        {          
            User user = await _userManager.GetUserAsync(User);

            var ord = repository.Orders.FirstOrDefault(x => x.OrderId == order.OrderId && x.UserId == user.Id);

            if (ord == null)
                return NotFound();
            else
            {
                if ((ord.DatetimeStartOrder - DateTime.Now).TotalHours < 12)
                    return RedirectToAction("Info", new { info = _localizer["Unfortunately, the time of order cancellation is out."] });
                else
                {
                    repository.SetCanceled(ord);
                    return RedirectToAction("Info", new { info = _localizer["Order"] + " №" + order.OrderId + " " + _localizer["successfully canceled"] + "." });
                }
            }
        }

        [HttpPost]
        public async Task<IActionResult> CanceledReserve(Order order)
        {
            User user = await _userManager.GetUserAsync(User);

            var ord = repository.Orders.FirstOrDefault(x => x.OrderId == order.OrderId && x.UserId == user.Id);

            if (ord == null)
                return NotFound();
            else
            {
                if ((ord.DatetimeStartOrder - DateTime.Now).TotalHours < 24)
                    return RedirectToAction("Info", new { info = _localizer["Your booking timed out, your booking has been canceled."] });
                else
                {
                    repository.SetCanceled(ord);
                    return RedirectToAction("Info", new { info = _localizer["Reservation"] + " №" + order.OrderId + " " + _localizer["was canceled"] + "." });
                }
            }
        }

        [HttpGet]
        public async Task<IActionResult> PaymentReserve(int id)
        {
            User user = await _userManager.GetUserAsync(User);

            var ord = repository.Orders.FirstOrDefault(x => x.OrderId == id && x.UserId == user.Id);

            if (ord == null)
                return NotFound();
            else
            {
                if ((ord.DatetimeStartOrder - DateTime.Now).TotalHours < 24)
                    return RedirectToAction("Info", new { info = _localizer["Your booking timed out, your booking has been canceled."] });
                else
                {
                    return View(ord);
                }
            }
        }

        [HttpPost]
        public async Task<IActionResult> PaymentReserve(Order order)
        {
            User user = await _userManager.GetUserAsync(User);

            var ord = repository.Orders.FirstOrDefault(x => x.OrderId == order.OrderId && x.UserId == user.Id);

            if (ord == null)
                return NotFound();
            else
            {
                if ((ord.DatetimeStartOrder - DateTime.Now).TotalHours < 24)
                    return RedirectToAction("Info", new { info = _localizer["Your booking timed out, your booking has been canceled."] });
                else
                {
                    ord.PaymentType = order.PaymentType;
                    repository.ReserveToOrder(ord);
                    return RedirectToAction("Info", new { info = _localizer["Reservation"] + " №" + order.OrderId + " " + _localizer["was successfully paid"] +"." });
                }
            }
        }

        public IActionResult Info(string info)
        {
            ViewBag.Info = info;
            return View();
        }

        public bool checkFreeTime(DateTime start, DateTime finish, int? carId)
        {
            var a = repository.Orders.Where(x => x.CarId == carId && !x.Status.Equals("canceled") && (
            (x.DatetimeFinishOrder >= start && x.DatetimeFinishOrder <= finish) ||
            (x.DatetimeStartOrder >= start && x.DatetimeStartOrder <= finish) ||
            (x.DatetimeStartOrder <= start && x.DatetimeFinishOrder >= finish)));

            if (a == null) return true;

            if (a.Count() == 0) return true;
            else return false;
        }
      
    }
}