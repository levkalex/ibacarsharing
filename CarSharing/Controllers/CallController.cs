﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CarSharing.Models.Database;
using CarSharing.Models.Project;
using Microsoft.AspNetCore.Mvc;

namespace CarSharing.Controllers
{
    public class CallController : Controller
    {
        private ICallRepository repository;
        public CallController(ICallRepository repo)
        {
            repository = repo;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Add(Call call)
        {
            repository.AddCall(call);
            return (PartialView());
        }

        [HttpPost]
        public IActionResult SetServer(Call call)
        {
            var edCall = repository.Calls.FirstOrDefault(x => x.CallID == call.CallID);
            edCall.Served = true;
            repository.UpdateCall(edCall);
          
            return (PartialView());
        }

        [HttpPost]
        public IActionResult Delete(Call call)
        {
            var delCall = repository.Calls.FirstOrDefault(x => x.CallID == call.CallID);
            repository.DeleteCall(delCall);

            return (PartialView());
        }
    }
}