﻿using CarSharing.Models;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarSharing.Controllers
{

    [Route("api/car/[action]")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [ApiController]
    public class CarApiController : ControllerBase
    {
        private readonly IRepository repository;

        public CarApiController(IRepository repository)
        {
            this.repository = repository;
        }

        [HttpGet]
        public ActionResult<IEnumerable<Car>> getcars(int count = 100, int offset = 0)
        {
            return repository.Cars.Skip(offset).Take(count).ToList();
        }

        [HttpGet("{id}")]
        public ActionResult<Car> getcar(long id)
        {
            var todoItem = repository.Cars.FirstOrDefault(x=>x.CarID == id);         

            if (todoItem == null)
            {
                return NotFound();
            }

            return todoItem;
        }

        [Authorize(Roles = "admin")]
        [HttpPost]
        public ActionResult<int> post(Car item)
        {
            var resp = repository.AddCar(item);
            return resp;
        }

        [HttpDelete("{id}")]
        [Authorize(Roles = "admin")]
        public IActionResult delete(long id)
        {
            var todoItem = repository.Cars.FirstOrDefault(x=>x.CarID == id);

            if (todoItem == null)
            {
                return NotFound();
            }

            repository.DeleteCar(todoItem);

            return NoContent();
        }

        [HttpPut("{id}")]
        [Authorize(Roles = "admin")]
        public IActionResult put(long id, Car item)
        {
            if (id != item.CarID)
            {
                return BadRequest();
            }

            repository.UpdateCar(item);

            return NoContent();
        }
    }
}
