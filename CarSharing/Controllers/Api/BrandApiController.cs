﻿using CarSharing.Models;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarSharing.Controllers
{

    [Route("api/brand/[action]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class BrandApiController : ControllerBase
    {
        private readonly IRepository repository;

        public BrandApiController(IRepository repository)
        {
            this.repository = repository;
        }

        [HttpGet]
        public ActionResult<IEnumerable<Brand>> getbrans(int count = 100, int offset = 0)
        {
            return repository.Brands.Skip(offset).Take(count).ToList();
        }

        [HttpGet("{id}")]
        public ActionResult<Brand> getbrand(long id)
        {
            var todoItem = repository.Brands.FirstOrDefault(x=>x.BrandID == id);         

            if (todoItem == null)
            {
                return NotFound();
            }

            return todoItem;
        }

        [Authorize(Roles = "admin")]
        [HttpPost]
        public ActionResult<int> post(Brand item)
        {
            var resp = repository.AddBrand(item);

            return resp;
        }

        [HttpDelete("{id}")]
        [Authorize(Roles = "admin")]
        public IActionResult delete(long id)
        {
            var todoItem = repository.Brands.FirstOrDefault(x=>x.BrandID == id);

            if (todoItem == null)
            {
                return NotFound();
            }

            repository.DeleteBrand(todoItem);

            return NoContent();
        }

        [HttpPut("{id}")]
        [Authorize(Roles = "admin")]
        public IActionResult put(long id, Brand item)
        {
            if (id != item.BrandID)
            {
                return BadRequest();
            }

            repository.UpdateBrand(item);

            return NoContent();
        }


    }
}
