﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using CarSharing.Models.Identity;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using static Microsoft.AspNetCore.Hosting.Internal.HostingApplication;

namespace CarSharing.Controllers.Api
{
    [Route("api/user")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class UserController : Controller
    {
        private readonly UserManager<User> userManager;
        public UserController(UserManager<User> userManager)
        {
            this.userManager = userManager;
        }

        [Route("getlogin")]
        [HttpGet]
        public IActionResult getLogin()
        {
            return Ok(User.Identity.Name);
        }

        [HttpGet]
        [Route("getusers")]
        [Authorize(Roles = "admin")]
        public ActionResult<IEnumerable<User>> getUsers(int count = 100, int offset = 0)
        {
            return userManager.Users.Skip(offset).Take(count).ToList();
        }

        [HttpGet]
        [Route("getuser")]
        [Authorize(Roles = "admin")]
        public ActionResult<User> getUser(int id)
        {
            return userManager.Users.FirstOrDefault(x => x.Id == id);
        }
    }
}