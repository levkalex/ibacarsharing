﻿using CarSharing.Models;
using CarSharing.Models.Database;
using CarSharing.Models.Identity;
using CarSharing.Models.Project;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarSharing.Controllers
{

    [Route("api/order/[action]")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [ApiController]
    public class OrderApiController : ControllerBase
    {
        private readonly IOrderRepository repository;
        private readonly UserManager<User> userManager;
        public OrderApiController(IOrderRepository repository, UserManager<User> userManager)
        {
            this.repository = repository;
            this.userManager = userManager;
        }

        [HttpGet]
        public ActionResult<IEnumerable<Order>> getorders(int count = 100, int offset = 0)
        {
            return repository.Orders.Where(x=>x.UserId == User.GetUserId()).Skip(offset).Take(count).ToList();
        }

        [HttpGet("{id}")]
        public ActionResult<Order> getorder(long id)
        {
            var todoItem = repository.Orders.FirstOrDefault(x=>x.OrderId == id && x.UserId == User.GetUserId());         

            if (todoItem == null)
            {
                if (User.IsInRole("admin"))
                {
                    var todoItemAdm = repository.Orders.FirstOrDefault(x => x.OrderId == id);
                    if (todoItemAdm != null) return todoItemAdm;
                }

                return NotFound();
            }

            return todoItem;
        }

        [HttpPost]
        [Authorize(Roles = "admin")]

        public ActionResult<int> post(Order item)
        {
            var resp = repository.AddOrder(item);
            if (resp != null)
                return resp;
            else
                return BadRequest();
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> canceledorder(long id)
        {
            User user = await userManager.GetUserAsync(User);

            var ord = repository.Orders.FirstOrDefault(x => x.OrderId == id && x.UserId == user.Id);

            if (ord == null)
                return NotFound();
            else
            {
                if ((ord.DatetimeStartOrder - DateTime.Now).TotalHours < 12)
                    return BadRequest();
                else
                {
                    repository.SetCanceled(ord);
                }
            }

            return NoContent();
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> canceledreserve(long id)
        {
            User user = await userManager.GetUserAsync(User);

            var ord = repository.Orders.FirstOrDefault(x => x.OrderId == id && x.UserId == user.Id);

            if (ord == null)
                return NotFound();
            else
            {
                if ((ord.DatetimeStartOrder - DateTime.Now).TotalHours < 24)
                    return BadRequest();
                else
                {
                    repository.SetCanceled(ord);
                }
            }

            return NoContent();
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> paymentreserve(long id, string paymentType)
        {
            User user = await userManager.GetUserAsync(User);


            var ord = repository.Orders.FirstOrDefault(x => x.OrderId == id && x.UserId == user.Id);

            if (ord == null)
                return NotFound();
            else
            {
                if ((ord.DatetimeStartOrder - DateTime.Now).TotalHours < 24)
                    return BadRequest();
                else
                {
                    ord.PaymentType = paymentType;
                    repository.ReserveToOrder(ord);
                }
            }

            return NoContent();
        }

        [HttpGet]
        public bool checkfreecar(DateTime start, DateTime finish, int? carId)
        {
            var a = repository.checkFreeTime(start, finish, carId);

            return a;
        }


    }
}
