﻿using Microsoft.AspNetCore.Mvc;
using CarSharing.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using System.IO;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using CarSharing.Models.Identity;
using Microsoft.AspNetCore.Localization;

namespace CarSharing.Controllers
{
    [ApiExplorerSettings(IgnoreApi = true)]
    public class HomeController : Controller
    {
        private IRepository repository;

        public HomeController(IRepository repo)
        {
            repository = repo;
        }

        public ViewResult Index()
        {
            return View(repository.Brands.Include(x => x.Cars));
        }
        public ViewResult Test()
        {
            return View();
        }


        [Route("Model/{id}")]
        [HttpGet]
        public PartialViewResult Car(int id)
        {
            return PartialView("Model", repository.Cars.AsQueryable().Where(x => x.Brand.BrandID == id));
        }

        [HttpPost]
        public IActionResult SetLanguage(string culture, string returnUrl)
        {
            Response.Cookies.Append(
                CookieRequestCultureProvider.DefaultCookieName,
                CookieRequestCultureProvider.MakeCookieValue(new RequestCulture(culture)),
                new CookieOptions { Expires = DateTimeOffset.UtcNow.AddYears(1) }
            );

            return LocalRedirect(returnUrl);
        }

        public IActionResult carousel()
        {
            return View();
        }
    }
}
