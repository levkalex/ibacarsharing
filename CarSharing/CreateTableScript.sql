CREATE TABLE Brands
(
    BrandID    serial primary key,
    Name        VARCHAR(40)
);

CREATE TABLE Cars
(
    CarID   	serial primary key,
    Name        VARCHAR(40),
	Price 		decimal,
	Description VARCHAR(40),
	BrandId 	INTEGER REFERENCES brands(brandid)
);

CREATE TABLE Calls
(
    CallID   	serial primary key,
    Name        VARCHAR(40),
	PhoneNumber VARCHAR(25),
	DesiredTime VARCHAR(12),
	Served 		BOOL
);