﻿$(document).ready(function () {
    $('#phone-number').mask('+375 (00) 000-00-00', { placeholder: "+375 (__) ___-__-__" }).attr('pattern', '.{19}');

    $('#time-phone').mask('Hh:Mm', {
        translation: {
            'H': {
                pattern: /[0-2]/, optional: true
            },
            'h': {
                pattern: /[0-9]/, optional: false
            },
            'M': {
                pattern: /[0-5]/, optional: false
            },
            'm': {
                pattern: /[0-9]/, optional: false
            }
        },
        onKeyPress: function (cep, event, currentField, options) {
            var inputText = $('#time-phone');
            var currentMask = inputText.val().replace(':', '');
            var intMask = parseInt(currentMask);

            if (isNaN(intMask)) {
                inputText.val("");
            } else if (intMask > 2359) {
                inputText.val("23:59");
            }
        }
    }
    );

    var showCallModelOk = false;

    $("#order-call").submit(function (event) {
        var posting = $.post('Call/Add', $("#order-call").serialize(),
            function (data) {
               

                $("#modelOk").html(data);
            });

        event.preventDefault();

        
        posting.done(function (data) {
            showCallModelOk = true;
            $('#callModel').modal('hide');
        });
    });

    $('#callModel').on('hidden.bs.modal', function () {
        $('#order-call').trigger('reset');
        if (showCallModelOk) {
            $("#callModelOk").modal("show");
            showCallModelOk = false;
        }
    });
    
});