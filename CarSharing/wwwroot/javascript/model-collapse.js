﻿$(document).ready(function () {
    $('.brandLine').click(function () {
        var blockCar = $(this).next();
        if (blockCar.css('display') == 'none') {
            blockCar.animate({ height: 'show' }, 900);
            $(this).addClass("activecol");
        }
        else {
            blockCar.animate({ height: 'hide' }, 500);
            $(this).removeClass("activecol");
        }
    });
});