﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarSharing.Helpers
{
    public static class ActiveMenuHelper
    {
        public static string IsSelected(this IHtmlHelper htmlHelper, string controllers, string actions, string areas = "", string cssClass = "btn btn-warning")
        {
            string currentAction = htmlHelper.ViewContext.RouteData.Values["action"] as string;
            string currentController = htmlHelper.ViewContext.RouteData.Values["controller"] as string;
            string currentArea = htmlHelper.ViewContext.RouteData.Values["area"] as string;

            IEnumerable<string> acceptedActions = (actions ?? currentAction).Split(',');
            IEnumerable<string> acceptedControllers = (controllers ?? currentController).Split(',');
            IEnumerable<string> acceptedAreas = (areas ?? currentArea).Split(',');

            return acceptedActions.Contains(currentAction) && acceptedControllers.Contains(currentController) && (acceptedAreas.Contains(currentArea) || areas == "") ?
                cssClass : String.Empty;
        }
    }
}
