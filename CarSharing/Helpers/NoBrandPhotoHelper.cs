﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace CarSharing.Helpers
{
    public static class NoBrandPhotoHelper
    {
        public static string GetPhotoBrand(this IHtmlHelper htmlHelper, int brandId)
        {
            bool b = File.Exists("wwwroot/images/brand/" + brandId + ".png");

            if (b)
                return ("images/brand/" + brandId + ".png");
            else
                return ("images/nophoto.jpg");
        }
    }
}
