﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace CarSharing.Helpers
{
    public static class NoCarPhotoHelper
    {
        public static string GetPhotoCar(this IHtmlHelper htmlHelper, int carId)
        {
            bool b = File.Exists("wwwroot/images/model/" + carId + ".jpg");

            if (b)
                return ("images/model/" + carId + ".jpg");
            else
                return ("images/nophoto.jpg");
        }
    }
}
