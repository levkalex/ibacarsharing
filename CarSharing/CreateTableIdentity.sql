CREATE TABLE "Users" (
    "Id" serial primary key,
    "UserName" character varying(256),
    "NormalizedUserName" character varying(256),
    "Email" character varying(256),
    "NormalizedEmail" character varying(256),
    "EmailConfirmed" boolean NOT NULL,
    "PasswordHash" text,
    "SecurityStamp" text,
    "ConcurrencyStamp" text,
    "PhoneNumber" text,
    "PhoneNumberConfirmed" boolean NOT NULL,
    "TwoFactorEnabled" boolean NOT NULL,
    "LockoutEnd" timestamp with time zone,
    "LockoutEnabled" boolean NOT NULL,
    "AccessFailedCount" integer NOT NULL
);


CREATE TABLE "UserTokens" (
    "UserId" serial primary key REFERENCES "Users"("Id"),
    "LoginProvider" text NOT NULL,
    "Name" text NOT NULL,
    "Value" text
);



CREATE TABLE "Roles" (
    "Id" serial primary key,
    "Name" character varying(256),
    "NormalizedName" character varying(256),
    "ConcurrencyStamp" text
);


CREATE TABLE "UserRoles" (
    "UserId" serial primary key REFERENCES "Users"("Id"),
    "RoleId" serial primary key REFERENCES "Roles"("Id")
);



CREATE TABLE "UserLogins" (
    "LoginProvider" serial primary key,
    "ProviderKey" serial primary key,
    "ProviderDisplayName" text,
    "UserId" integer NOT NULL REFERENCES "Users"("Id")
);


CREATE TABLE "UserClaims" (
    "Id" serial primary key,
    "UserId" integer NOT NULL REFERENCES "Users"("Id"),
    "ClaimType" text,
    "ClaimValue" text
);



CREATE TABLE "RoleClaims" (
    "Id" serial primary key,
    "RoleId" integer REFERENCES "Roles"("Id"),
    "ClaimType" text,
    "ClaimValue" text
);

