﻿using CarSharing.Models.Database;
using CarSharing.Models.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarSharing.Components
{
    public class FreeDays : ViewComponent
    {
        IOrderRepository OrderRepository;

        public FreeDays(IOrderRepository repository)
        {
            OrderRepository = repository;
        }

        public IViewComponentResult Invoke(int id)
        {
            IEnumerable<BusyDaysViewModel> busyDays = OrderRepository.Orders.Take(1).Select(y => new BusyDaysViewModel{ Day = RoundUpDay(DateTime.Now), Count = OrderRepository.Orders.Where(x => x.CarId == id && x.Status != "canceled" && ((x.DatetimeFinishOrder >= RoundUpDay(DateTime.Now) && x.DatetimeFinishOrder <= RoundUpDay(DateTime.Now.AddDays(1))) ||
            (x.DatetimeStartOrder >= RoundUpDay(DateTime.Now) && x.DatetimeStartOrder <= RoundUpDay(DateTime.Now.AddDays(1))) ||
            (x.DatetimeStartOrder <= RoundUpDay(DateTime.Now) && x.DatetimeFinishOrder >= RoundUpDay(DateTime.Now.AddDays(1))))).Count() })
            .Union(OrderRepository.Orders.Take(1).Select(y => new BusyDaysViewModel{ Day = RoundUpDay(DateTime.Now.AddDays(1)), Count = OrderRepository.Orders.Where(x => x.CarId == id && x.Status != "canceled" && ((x.DatetimeFinishOrder >= RoundUpDay(DateTime.Now.AddDays(1)) && x.DatetimeFinishOrder <= RoundUpDay(DateTime.Now.AddDays(2))) ||
            (x.DatetimeStartOrder >= RoundUpDay(DateTime.Now.AddDays(1)) && x.DatetimeStartOrder <= RoundUpDay(DateTime.Now.AddDays(2))) ||
            (x.DatetimeStartOrder <= RoundUpDay(DateTime.Now.AddDays(1)) && x.DatetimeFinishOrder >= RoundUpDay(DateTime.Now.AddDays(2))))).Count() }).Distinct())
             .Union(OrderRepository.Orders.Take(1).Select(y => new BusyDaysViewModel{ Day = RoundUpDay(DateTime.Now.AddDays(2)), Count = OrderRepository.Orders.Where(x => x.CarId == id && x.Status != "canceled" && ((x.DatetimeFinishOrder >= RoundUpDay(DateTime.Now.AddDays(2)) && x.DatetimeFinishOrder <= RoundUpDay(DateTime.Now.AddDays(3))) ||
            (x.DatetimeStartOrder >= RoundUpDay(DateTime.Now.AddDays(2)) && x.DatetimeStartOrder <= RoundUpDay(DateTime.Now.AddDays(3))) ||
            (x.DatetimeStartOrder <= RoundUpDay(DateTime.Now.AddDays(2)) && x.DatetimeFinishOrder >= RoundUpDay(DateTime.Now.AddDays(3))))).Count() }).Distinct())
              .Union(OrderRepository.Orders.Take(1).Select(y => new BusyDaysViewModel{ Day = RoundUpDay(DateTime.Now.AddDays(3)), Count = OrderRepository.Orders.Where(x => x.CarId == id && x.Status != "canceled" && ((x.DatetimeFinishOrder >= RoundUpDay(DateTime.Now.AddDays(3)) && x.DatetimeFinishOrder <= RoundUpDay(DateTime.Now.AddDays(4))) ||
            (x.DatetimeStartOrder >= RoundUpDay(DateTime.Now.AddDays(3)) && x.DatetimeStartOrder <= RoundUpDay(DateTime.Now.AddDays(4))) ||
            (x.DatetimeStartOrder <= RoundUpDay(DateTime.Now.AddDays(3)) && x.DatetimeFinishOrder >= RoundUpDay(DateTime.Now.AddDays(4))))).Count() }).Distinct())
              .Union(OrderRepository.Orders.Take(1).Select(y => new BusyDaysViewModel{ Day = RoundUpDay(DateTime.Now.AddDays(4)), Count = OrderRepository.Orders.Where(x => x.CarId == id && x.Status != "canceled" && ((x.DatetimeFinishOrder >= RoundUpDay(DateTime.Now.AddDays(4)) && x.DatetimeFinishOrder <= RoundUpDay(DateTime.Now.AddDays(5))) ||
            (x.DatetimeStartOrder >= RoundUpDay(DateTime.Now.AddDays(4)) && x.DatetimeStartOrder <= RoundUpDay(DateTime.Now.AddDays(5))) ||
            (x.DatetimeStartOrder <= RoundUpDay(DateTime.Now.AddDays(4)) && x.DatetimeFinishOrder >= RoundUpDay(DateTime.Now.AddDays(5))))).Count() }).Distinct())
               .Union(OrderRepository.Orders.Take(1).Select(y => new BusyDaysViewModel{ Day = RoundUpDay(DateTime.Now.AddDays(5)), Count = OrderRepository.Orders.Where(x => x.CarId == id && x.Status != "canceled" && ((x.DatetimeFinishOrder >= RoundUpDay(DateTime.Now.AddDays(5)) && x.DatetimeFinishOrder <= RoundUpDay(DateTime.Now.AddDays(6))) ||
            (x.DatetimeStartOrder >= RoundUpDay(DateTime.Now.AddDays(5)) && x.DatetimeStartOrder <= RoundUpDay(DateTime.Now.AddDays(6))) ||
            (x.DatetimeStartOrder <= RoundUpDay(DateTime.Now.AddDays(5)) && x.DatetimeFinishOrder >= RoundUpDay(DateTime.Now.AddDays(6))))).Count() }).Distinct())
               .Union(OrderRepository.Orders.Take(1).Select(y => new BusyDaysViewModel{ Day = RoundUpDay(DateTime.Now.AddDays(6)), Count = OrderRepository.Orders.Where(x => x.CarId == id && x.Status != "canceled" && ((x.DatetimeFinishOrder >= RoundUpDay(DateTime.Now.AddDays(6)) && x.DatetimeFinishOrder <= RoundUpDay(DateTime.Now.AddDays(7))) ||
            (x.DatetimeStartOrder >= RoundUpDay(DateTime.Now.AddDays(6)) && x.DatetimeStartOrder <= RoundUpDay(DateTime.Now.AddDays(7))) ||
            (x.DatetimeStartOrder <= RoundUpDay(DateTime.Now.AddDays(6)) && x.DatetimeFinishOrder >= RoundUpDay(DateTime.Now.AddDays(7))))).Count() }).Distinct());

            return View(busyDays);
        }

        DateTime RoundUpDay(DateTime dt)
        {
            TimeSpan d = TimeSpan.FromHours(24);
            return new DateTime(((dt.Ticks + d.Ticks - 1) / d.Ticks) * d.Ticks);
        }
    }
}
