﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CarSharing.Areas.Admin.Controllers;
using Microsoft.AspNetCore.Mvc;

namespace CarSharing.Areas.Admin
{
    public class HomeController : AdminController
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}