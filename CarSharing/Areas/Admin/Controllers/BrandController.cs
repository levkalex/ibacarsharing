﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using CarSharing.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CarSharing.Areas.Admin.Controllers
{
    public class BrandController : AdminController
    {
        private IRepository repository;

        public BrandController(IRepository repo)
        {
            repository = repo;
        }

        public IActionResult List()
        {
            return View(repository.Brands.Include(x => x.Cars));
        }
         
        [HttpGet]
        public IActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Add(Brand brand, IFormFile file)
        {
            int id = repository.AddBrand(brand);

            if (file != null)
            {
                string path = @"wwwroot/images/brand/" + id + ".png";
                using (var fileStream = new FileStream(path, FileMode.Create))
                {
                    await file.CopyToAsync(fileStream);
                }
            }

            return RedirectToAction("List");
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            Brand brand = repository.Brands.FirstOrDefault(x => x.BrandID == id);

            if (brand == null)
                return NotFound();
            else
                repository.DeleteBrand(brand);

            string path = @"wwwroot/images/brand/" + id + ".png";
            if (System.IO.File.Exists(path))
            {
                System.IO.File.Delete(path);
            }

            return RedirectToAction("List");
        }

        [HttpGet]
        public ViewResult Edit(int id)
        {
            Brand brand = repository.Brands.FirstOrDefault(x => x.BrandID == id);

            return View(brand);
        }

        [HttpPost]
        public async Task<ActionResult> Edit(Brand brand, IFormFile file)
        {
            if (file != null)
            {
                string path = @"wwwroot/images/brand/" + brand.BrandID + ".png";

                using (var fileStream = new FileStream(path, FileMode.Create))
                {
                    await file.CopyToAsync(fileStream);
                }
            }

            repository.UpdateBrand(brand);

            return RedirectToAction("List");
        }
    }
}