﻿using CarSharing.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace CarSharing.Areas.Admin.Controllers
{
    public class CarController : AdminController
    {
        private IRepository repository;

        public CarController(IRepository repo)
        {
            repository = repo;
        }

        public IActionResult List()
        {
            return View(repository.Cars.Include(x => x.Brand));
        }

        [HttpGet]
        public ViewResult Add()
        {
            var brands = repository.Brands;
            return View(brands);
        }

        [HttpPost]
        public async Task<ActionResult> Add(Car car, IFormFile file)
        {
            int id = repository.AddCar(car);

            if (file != null)
            {
                string path = @"wwwroot/images/model/" + id + ".jpg";
                using (var fileStream = new FileStream(path, FileMode.Create))
                {
                    await file.CopyToAsync(fileStream);
                }
            }

            return RedirectToAction("List");
        }


        [HttpGet]
        public ViewResult Edit(int id)
        {
            Car car = repository.Cars.AsQueryable().Where(x => x.CarID == id).FirstOrDefault();
            var brands = repository.Brands;

            return View(new CarWithBrand(car, brands));
        }


        [HttpPost]
        public async Task<ActionResult> Edit(Car car, IFormFile file)
        {
            if (file != null)
            {
                string path = @"wwwroot/images/model/" + car.CarID + ".jpg";

                using (var fileStream = new FileStream(path, FileMode.Create))
                {
                    await file.CopyToAsync(fileStream);
                }
            }

            repository.UpdateCar(car);

            return RedirectToAction("List");
        }


        [HttpPost]
        public ActionResult Delete(int id, string prewUrl = "")
        {
            Car car = repository.Cars.FirstOrDefault(x => x.CarID == id);

            if (car == null)
                return NotFound();
            else
                repository.DeleteCar(car);

            string path = @"wwwroot/images/model/" + id + ".jpg";
            if (System.IO.File.Exists(path))
            {
                System.IO.File.Delete(path);
            }

            if (prewUrl == "")
                return RedirectToAction("List");
            else
                return Redirect(prewUrl);
        }
    }
}








