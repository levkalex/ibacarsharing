﻿using CarSharing.Models.Database;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;

namespace CarSharing.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles="admin")]
    public class OrderController : Controller
    {
        private IOrderRepository _orderRepository;

        public OrderController(IOrderRepository orderRepository)
        {
            _orderRepository = orderRepository;
        }

        public async Task<IActionResult> All()
        {
            return View(_orderRepository.Orders);
        }

        public async Task<IActionResult> WaitCheck()
        {
            return View(_orderRepository.Orders.Where(x => x.Status == "wait-approve"));
        }
    }
}
