﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CarSharing.Models.Database;
using Microsoft.AspNetCore.Mvc;

namespace CarSharing.Areas.Admin.Controllers
{
    public class CallController : AdminController
    {
        ICallRepository callRepository;
        public CallController(ICallRepository callRepository)
        {
            this.callRepository = callRepository;
        }

        public IActionResult List(string type = "waiting")
        {
            ViewData["type"] = type;
            if (type == "proccesed")
            {
                var calls = callRepository.Calls.Where(x => x.Served == true).OrderByDescending(x => x.CallID); ;
                return View("Proccesed", calls);
            }
            if (type == "all")
            {
                var calls = callRepository.Calls.OrderByDescending(x => x.CallID); ;
                return View("All", calls);
            }
            else
            {
                var calls = callRepository.Calls.Where(x => x.Served == false).OrderByDescending(x => x.CallID);
                return View("Waiting", calls);
            }
        }
    }
}