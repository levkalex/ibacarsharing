﻿using CarSharing.Models.Identity;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarSharing.Areas.Admin.Components
{
    public class CheckUserIsAdmin : ViewComponent
    {
        private readonly UserManager<User> userManager;
        private readonly SignInManager<User> signInManager;

        public CheckUserIsAdmin(UserManager<User> userManager, SignInManager<User> signInManager)
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
        }

        public async Task<string> InvokeAsync(User user)
        {
            //var test = await userManager.IsInRoleAsync(user, "moder");

            var ch = await userManager.GetRolesAsync(user);
            if (ch.Contains("admin"))
                return "checked";
            else
                return String.Empty;
        }
    }
}
