﻿using CarSharing.Areas.Admin.Models.Components;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarSharing.Areas.Admin.Models.ViewModels
{
   
    public class FileInput : ViewComponent
    {
        public IViewComponentResult Invoke(string accept = "", int maxHeight = 200, string text = "Choose image")
        {
            return View(new FileInputParams() { Accept = accept, MaxHeight = maxHeight, Text = text });
        }
    }
}
