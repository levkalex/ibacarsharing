﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarSharing.Areas.Admin.Models.Components
{
    public class FileInputParams
    {
        public string Accept { get; set; } = "";
        public int MaxHeight { get; set; } = 200;
        public string Text { get; set; } = "Choose image";
    }
}
