﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarSharing.Models.Project
{
    public class Call
    {
        public int CallID { get; set; }
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public string DesiredTime { get; set; }
        public bool Served { get; set; }
    }
}   
