﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarSharing.Models
{
    public class Brand
    {
        public int BrandID { get; set; }
        public string Name { get; set; }

        public ICollection<Car> Cars { get; set; }
        public Brand()
        {
            Cars = new List<Car>();
        }
    }
}
