﻿using CarSharing.Models.Project;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarSharing.Models
{
    public class Car
    {
        public int CarID { get; set; }
        public string Name { get; set; }
        public decimal PriceHour { get; set; }
        public decimal PriceDay { get; set; }
        public string Description { get; set; }
        public string Type { get; set; }

        public string Trasition { get; set; }
        public int AirBags { get; set; }
        public string Technology { get; set; }
        public double Economy { get; set; }
        public string FuelType { get; set; }
        public int Power { get; set; }
        public int Seats { get; set; }
        public int Capity { get; set; }
        public int ProductYear { get; set; }
        public int Odometer { get; set; }

        public int? BrandId { get; set; }
        public Brand Brand { get; set; }

        public ICollection<Order> Orders { get; set; }
        public Car()
        {
            Orders = new List<Order>();
        }
    }
}
