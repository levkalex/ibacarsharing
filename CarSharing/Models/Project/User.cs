﻿using CarSharing.Models.Project;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarSharing.Models.Identity
{
    public class User : IdentityUser<int>
    {
        public DateTime Birthday { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public ICollection<Order> Orders { get; set; }
        public User()
        {
            Orders = new List<Order>();
        }

        public User(string userName) : base(userName)
        {
            Orders = new List<Order>();
        }
    }
}
