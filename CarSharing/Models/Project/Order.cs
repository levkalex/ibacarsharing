﻿using CarSharing.Models.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarSharing.Models.Project
{
    public class Order
    {
        public int OrderId { get; set; }

        public int? UserId { get; set; }
        public User User { get; set; }

        public int? CarId { get; set; }
        public Car Car { get; set; }

        public string TypeOrder { get; set; }
        public string Status { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PaymentType { get; set; }

        public string Mail { get; set; }
        public string Phone { get; set; }
        public string Note { get; set; }

        public DateTime DateTimeOrder { get; set; }
        public DateTime DatetimeStartOrder { get; set; }
        public DateTime DatetimeFinishOrder { get; set; }

        public string Tariff { get; set; }

        public int RentalTime { get; set; }

        public decimal UnitCost { get; set; }
        public decimal TotalCost { get; set; }
    }
}
