﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarSharing.Models
{
    public class CarWithBrand
    {
        public Car Car { get; set; }
        public IEnumerable<Brand> Brands { get; set; }

        public CarWithBrand(Car car, IEnumerable<Brand> brands)
        {
            Car = car;
            Brands = brands;
        }
    }
}
