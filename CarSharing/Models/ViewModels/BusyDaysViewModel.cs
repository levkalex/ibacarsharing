﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarSharing.Models.ViewModels
{
    public class BusyDaysViewModel
    {
        public DateTime Day { get; set; }
        public int Count { get; set; }
    }
}
