﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarSharing.Models.ViewModels
{
    public class ListCarViewModel
    {
        public IEnumerable<Car> Cars { get; set; }
        public IQueryable<Brand> Brands { get; set; }
        public IQueryable<string> Types { get; set; }
        public string CurrentBrand { get; set; }
        public string CurrentType { get; set; }
    }
}
