﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarSharing.Models.Identity
{
    public class AuthOptions
    {
        public const string ISSUER = "RentalCarApp";
        public const string AUDIENCE = "http://localhost:44388/";
        const string KEY = "student password";
        public const int LIFETIME = 1;
        public static SymmetricSecurityKey GetSymmetricSecurityKey()
        {
            return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(KEY));
        }
    }
}
