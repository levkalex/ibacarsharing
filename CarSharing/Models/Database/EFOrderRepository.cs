﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CarSharing.Models.Project;
using Microsoft.EntityFrameworkCore;

namespace CarSharing.Models.Database
{
    public class EFOrderRepository : IOrderRepository
    {
        private ApplicationDbContext context;
        static DateTime lastUpdate = DateTime.Now.AddDays(-1);

        public EFOrderRepository(ApplicationDbContext ctx)
        {
            context = ctx;

            if (((DateTime.Now - lastUpdate).TotalMinutes > 1) || (DateTime.Now.Minute != lastUpdate.Minute))
            {
                CheckAllOrder();
                lastUpdate = DateTime.Now;
            }

        }

        public IQueryable<Order> Orders => context.Orders.Include(x=>x.Car).Include(x=>x.Car.Brand).Include(x=>x.User);

        public int? AddOrder(Order order)
        {
            //count days and hours
            if (order.Tariff == "hour") {
                order.RentalTime = (int)Math.Ceiling((order.DatetimeFinishOrder - order.DatetimeStartOrder).TotalHours);
                order.UnitCost = context.Cars.FirstOrDefault(x => x.CarID == order.CarId).PriceHour;
            }
            else
            {
                order.RentalTime = (int)Math.Ceiling((order.DatetimeFinishOrder - order.DatetimeStartOrder).TotalDays);
                order.UnitCost = context.Cars.FirstOrDefault(x => x.CarID == order.CarId).PriceDay;
            }


            order.TotalCost = Math.Round(order.UnitCost * order.RentalTime, 2);

            if (order.TypeOrder == "order")
            {
                if ((order.DatetimeStartOrder - DateTime.Now).TotalHours > 12)
                    order.Status = "return-possible";
                else
                    order.Status = "return-impossible";
            }
            else if (order.TypeOrder == "reserve")
            {
                order.Status = "waiting-payment";
                order.PaymentType = null;
            }

            context.Add(order);
            context.SaveChanges();

            return order.OrderId;
        }

        public void CheckAllOrder()
        {
            CheckReserveCancel();
            CheckOrderImposibleReturn();
            CheckOrderInProgress();
            CheckOrderWaitApprove();
        }

        public void CheckReserveCancel()
        {
            var orders = Orders.Where(x => x.TypeOrder == "reserve" && x.Status == "waiting-payment" && (x.DatetimeStartOrder - DateTime.Now).TotalHours < 24).AsEnumerable().Select(c => { c.Status = "canceled"; return c; });

            foreach (Order order in orders)
                context.Entry(order).State = EntityState.Modified;

            context.SaveChanges();
        }

        public void CheckOrderImposibleReturn()
        {
            var orders = Orders.Where(x => x.TypeOrder == "order" && x.Status == "return-possible" && (x.DatetimeStartOrder - DateTime.Now).TotalHours < 12 && x.DatetimeStartOrder > DateTime.Now).AsEnumerable().Select(c => { c.Status = "return-impossible"; return c; });

            foreach (Order order in orders)
                context.Entry(order).State = EntityState.Modified;

            context.SaveChanges();
        }

        public void CheckOrderInProgress()
        {
           var orders = Orders.Where(x => x.TypeOrder == "order" && (x.Status == "return-possible" || x.Status == "return-impossible") && (x.DatetimeStartOrder < DateTime.Now)).AsEnumerable().Select(c => { c.Status = "in-progress"; return c; });

            foreach (Order order in orders)
                context.Entry(order).State = EntityState.Modified;

            context.SaveChanges();
        }

        public void CheckOrderWaitApprove()
        {
            var orders = Orders.Where(x => x.TypeOrder == "order" && (x.Status == "in-progress") && (x.DatetimeFinishOrder < DateTime.Now)).AsEnumerable().Select(c => { c.Status = "wait-approve"; return c; });

            foreach (Order order in orders)
                context.Entry(order).State = EntityState.Modified;

            context.SaveChanges();
        }

        public void SetCanceled(Order order)
        {
            order.Status = "canceled";
            context.SaveChanges();
        }


        
        public void ReserveToOrder(Order order)
        {
            order.TypeOrder = "order";
            order.Status = "return-possible";
            context.SaveChanges();
        }


        public bool checkFreeTime(DateTime start, DateTime finish, int? carId)
        {
            var a = Orders.Where(x => x.CarId == carId && !x.Status.Equals("canceled") && (
            (x.DatetimeFinishOrder >= start && x.DatetimeFinishOrder <= finish) ||
            (x.DatetimeStartOrder >= start && x.DatetimeStartOrder <= finish) ||
            (x.DatetimeStartOrder <= start && x.DatetimeFinishOrder >= finish)));

            if (a == null) return true;

            if (a.Count() == 0) return true;
            else return false;
        }


    }
}
