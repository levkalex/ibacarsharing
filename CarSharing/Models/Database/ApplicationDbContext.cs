﻿using CarSharing.Models.Identity;
using CarSharing.Models.Project;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;

namespace CarSharing.Models
{
    public class ApplicationDbContext : IdentityDbContext<User, Role, int>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options) { }

        public DbSet<Brand> Brands { get; set; }
        public DbSet<Car> Cars { get; set; }
        public DbSet<Call> Calls { get; set; }
        public DbSet<Order> Orders { get; set; }
        public new DbSet<User> Users { get; set; }
        public new DbSet<Role> Roles { get; set; }
        public new DbSet<IdentityUserClaim<int>> UserClaims { get; set; }
        public new DbSet<IdentityUserLogin<int>> UserLogins { get; set; }
        public new DbSet<IdentityUserRole<int>> UserRoles { get; set; }
        public new DbSet<IdentityRoleClaim<int>> RoleClaims { get; set; }
        public new DbSet<IdentityUserRole<int>> UserTokens { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}
