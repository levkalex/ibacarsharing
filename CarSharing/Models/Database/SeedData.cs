﻿using System.Linq;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using CarSharing.Models.Project;

namespace CarSharing.Models
{

    public static class SeedData
    {

        public static void EnsurePopulated(IApplicationBuilder app)
        {

            ApplicationDbContext context = app.ApplicationServices.GetRequiredService<ApplicationDbContext>();       

           // context.Database.Migrate();
            if (!context.Brands.Any())
            {
                Brand audi = new Brand { Name = "Audi" };
                Brand bmw = new Brand { Name = "BMW" };
                Brand tesla = new Brand { Name = "Tesla" };

                context.Brands.AddRange(new List<Brand> { audi, bmw, tesla });

                Car a8 = new Car { Name = "A8", Description = "New audi a8!", PriceDay = 23.00M, PriceHour = 2.00M, Brand = audi };
                Car a4 = new Car { Name = "A4", Description = "New audi a4!", PriceDay = 13.00M, PriceHour = 2.00M, Brand = audi };
                Car i328 = new Car { Name = "328i", Description = "New bmw beautiful!", PriceDay = 33.00M, PriceHour = 2.00M, Brand = bmw };
                Car models = new Car { Name = "Model S", Description = "New Electro Tesla!", PriceDay = 53.00M, PriceHour = 2.00M, Brand = tesla };

                context.Cars.AddRange(new List<Car> { a8, a4, i328, models });

                context.SaveChanges();
            }

            if (!context.Calls.Any())
            {
                Call call = new Call { Name = "Ivan", DesiredTime = "12:00", PhoneNumber = "+375 (29) 274 61 53", Served = false };
                Call call2 = new Call { Name = "Vasya", PhoneNumber = "+375 (29) 762 83 67", Served = false };
                Call call3 = new Call { Name = "Petya", DesiredTime = "18:20", PhoneNumber = "+375 (29) 123 45 67", Served = false };

                context.Calls.AddRange(new List<Call> { call, call2, call3 });

                context.SaveChanges();
            }
        }
    }
}
