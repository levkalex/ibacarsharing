﻿using CarSharing.Models.Project;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarSharing.Models.Database
{
    public interface ICallRepository
    {
        IQueryable<Call> Calls { get; }

        int AddCall(Call call);
        void DeleteCall(Call call);
        void UpdateCall(Call call);
    }
}
