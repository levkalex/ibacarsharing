﻿using CarSharing.Models.Project;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarSharing.Models.Database
{
    public interface IOrderRepository
    {
        IQueryable<Order> Orders { get; }
        int? AddOrder(Order order);
        void SetCanceled(Order order);
        void ReserveToOrder(Order order);

        bool checkFreeTime(DateTime start, DateTime finish, int? carId);
    }
}
