﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarSharing.Models
{
    public interface IRepository
    {
        IQueryable<Brand> Brands { get; }
        IQueryable<Car> Cars { get; }

        int AddBrand(Brand brand);
        int AddCar(Car car);
        void DeleteCar(Car car);
        void UpdateCar(Car car);
        void DeleteBrand(Brand brand);
        void UpdateBrand(Brand brand);
    }
}
