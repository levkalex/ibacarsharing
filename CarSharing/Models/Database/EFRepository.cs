﻿using System.Linq;

namespace CarSharing.Models
{
    public class EFRepository : IRepository
    {
        private ApplicationDbContext _dbContext;

        public EFRepository(ApplicationDbContext ctx)
        {
            _dbContext = ctx;
        }

        public IQueryable<Brand> Brands => _dbContext.Brands;

        public IQueryable<Car> Cars => _dbContext.Cars;

        public int AddBrand(Brand brand)
        {
            _dbContext.Add(brand);
            _dbContext.SaveChanges();

            return brand.BrandID;
        }

        public int AddCar(Car car)
        {
            _dbContext.Add(car);
            _dbContext.SaveChanges();

            return car.CarID;
        }

        public void DeleteBrand(Brand brand)
        {
            var cars = _dbContext.Cars.AsQueryable().Where(x => x.BrandId == brand.BrandID);
            _dbContext.Cars.RemoveRange(cars);

            _dbContext.Brands.Remove(brand);
            _dbContext.SaveChanges();
        }

        public void DeleteCar(Car car)
        {
            _dbContext.Cars.Remove(car);
            _dbContext.SaveChanges();
        }

        public void UpdateBrand(Brand brand)
        {
            _dbContext.Brands.Update(brand);
            _dbContext.SaveChanges();
        }

        public void UpdateCar(Car car)
        {
            _dbContext.Cars.Update(car);
            _dbContext.SaveChanges();
        }
    }
}
