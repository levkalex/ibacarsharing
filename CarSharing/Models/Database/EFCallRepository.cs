﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CarSharing.Models.Project;

namespace CarSharing.Models.Database
{
    public class EFCallRepository : ICallRepository
    {
        private ApplicationDbContext context;

        public EFCallRepository(ApplicationDbContext ctx)
        {
            context = ctx;
        }

        public IQueryable<Call> Calls => context.Calls;

        public int AddCall(Call call)
        {
            context.Add(call);
            context.SaveChanges();

            return call.CallID;
        }

        public void DeleteCall(Call call)
        {
            context.Calls.Remove(call);
            context.SaveChanges();
        }

        public void UpdateCall(Call call)
        {
            context.Calls.Update(call);
            context.SaveChanges();
        }
    }
}
