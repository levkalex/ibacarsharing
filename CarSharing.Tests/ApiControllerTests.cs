﻿using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using CarSharing.Models;
using System.Linq;
using CarSharing.Controllers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using CarSharing.Models.Identity;
using CarSharing.Models.Database;
using CarSharing.Models.Project;
using System.Threading;
using Microsoft.AspNetCore.Http;
using System.Security.Claims;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Authentication;

namespace CarSharing.Tests
{
    public class ApiControllerTests
    {
        public ApiControllerTests()
        {

        }

        static public Mock<IRepository> BrandsCarsData()
        {
            Mock<IRepository> mock = new Mock<IRepository>();
            mock.Setup(m => m.Brands).Returns((new Brand[]
            {
                   new Brand { BrandID = 1, Name = "Audi" },
                    new Brand {  BrandID = 2, Name = "BMW" },
                     new Brand { BrandID = 3,  Name = "Tesla" }
            }).AsQueryable<Brand>());

            mock.Setup(m => m.Cars).Returns((new Car[]
            {
                  new Car { CarID = 1, Name = "A8", Description = "New audi a8!", PriceDay = 23.00M, Brand = mock.Object.Brands.ElementAt(0)},
                  new Car { CarID = 2,Name = "A4", Description = "New audi a4!", PriceDay = 13.00M, Brand = mock.Object.Brands.ElementAt(0)},
                  new Car { CarID = 3,Name = "328i", Description = "New bmw beautiful!", PriceDay = 33.00M, Brand = mock.Object.Brands.ElementAt(1)},
                  new Car { CarID = 4,Name = "Model S", Description = "New Electro Tesla!", PriceDay = 53.00M, Brand = mock.Object.Brands.ElementAt(2)}
            }).AsQueryable<Car>());

            return mock;
        }

        static public Mock<IOrderRepository> OrdersData()
        {
            Mock<IOrderRepository> mock = new Mock<IOrderRepository>();
            mock.Setup(m => m.Orders).Returns((new Order[]
            {
                   new Order
                   {
                       OrderId = 1,
                       CarId = 2,
                       Mail = "test@mail.ru",
                       DateTimeOrder = DateTime.Now,
                       DatetimeStartOrder = DateTime.Now.AddDays(1),
                       DatetimeFinishOrder = DateTime.Now.AddDays(4),
                       UserId = 1
                   },
                   new Order
                   {
                       OrderId = 2,
                       CarId = 2,
                       Mail = "test@mail.ru",
                       DateTimeOrder = DateTime.Now,
                       DatetimeStartOrder = DateTime.Now.AddDays(1),
                       DatetimeFinishOrder = DateTime.Now.AddDays(4),
                       UserId = 3
                   },
                   new Order
                   {
                       OrderId = 3,
                       CarId = 2,
                       Mail = "test@mail.ru",
                       DateTimeOrder = DateTime.Now,
                       DatetimeStartOrder = DateTime.Now.AddDays(1),
                       DatetimeFinishOrder = DateTime.Now.AddDays(4),
                       UserId = 3
                   }
            }).AsQueryable<Order>());

            return mock;
        }

        public ControllerContext ContextWithUser(int id)
        {
            var context = new DefaultHttpContext
            {
                User = new ClaimsPrincipal(
                    new ClaimsIdentity(new List<Claim>()
            {
                new Claim(ClaimTypes.Name, "grava_a@mail.ru"),
                new Claim(ClaimTypes.NameIdentifier, id.ToString())
            }, "TestAuthType"))
            };

            return new ControllerContext { HttpContext = context };
        }

        public static UserManager<User> userManager()
        {
            var store = new Mock<IUserStore<User>>();
            var mgr = new UserManager<User>(store.Object, null, null, null, null, null, null, null, null);

            var user = new User() { Email = "test@test.ru", UserName = "name" };
            mgr.CreateAsync(user, "testpas");

            return mgr;
        }


        [Fact]
        public void GetOrders()
        {

            //Организация
            OrderApiController controller = new OrderApiController(OrdersData().Object, userManager());

            //Действие
            controller.ControllerContext = ContextWithUser(3);
            var orders = controller.getorders(100);

            //Утверждение
            Assert.True(orders.Value.Count() == 2);
        }


        [Fact]
        public void GetOrder()
        {
            var ordersData = OrdersData().Object;
            //Организация
            OrderApiController controller = new OrderApiController(ordersData, userManager());

            //Действие
            controller.ControllerContext = ContextWithUser(1);
            var order = controller.getorder(1);

            controller.ControllerContext = ContextWithUser(3);
            var order2 = controller.getorder(1);

            //Утверждение
            Assert.NotNull(order.Value);
            Assert.Null(order2.Value);
            Assert.True(order.Value == ordersData.Orders.FirstOrDefault(x=>x.OrderId ==1));
        }


        [Fact]
        public void GetCars()
        {
            var carsData = BrandsCarsData().Object;
            //Организация
            CarApiController controller = new CarApiController(carsData);

            //Действие
            var cars = controller.getcars(4);

            //Утверждение
            Assert.NotNull(cars.Value);
            Assert.True(cars.Value.Count() == 4);
        }

        [Fact]
        public void GetBrand()
        {
            var carsData = BrandsCarsData().Object;
            //Организация
            BrandApiController controller = new BrandApiController(carsData);

            //Действие
            var brand = controller.getbrand(2);

            //Утверждение
            Assert.NotNull(brand.Value);
            Assert.True(brand.Value.Name == "BMW");
        }
    }
}
